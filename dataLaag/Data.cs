﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dataLaag
{
    public class Data
    {
        
        public List<string> HaalFacturenOp()
        {
            List<string> facturen = new List<string>();
            string pad = @"C:\ODISEE\FASE 2\.NET\IL2\dataLaag\data\facturen.txt";

            try
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader(pad))
                {
                    string regel;
                    while ((regel = sr.ReadLine()) != null)
                    {
                        facturen.Add(regel);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Fout bij het lezen van het bestand: " + e.Message);
            }
            return facturen;
        }

        public List<string> HaalProductenOp()
        {
            List<string> facturen = new List<string>();
            string pad = @"C:\ODISEE\FASE 2\.NET\IL2\dataLaag\data\producten.txt";

            try
            {
                using (System.IO.StreamReader sr = new System.IO.StreamReader(pad))
                {
                    string regel;
                    while ((regel = sr.ReadLine()) != null)
                    {
                        facturen.Add(regel);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Fout bij het lezen van het bestand: " + e.Message);
            }
            return facturen;
        }
    }
}
