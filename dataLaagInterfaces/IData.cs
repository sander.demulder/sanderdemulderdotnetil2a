﻿using models;

namespace datalaaginterfaces
{
    public interface IData
    {
        List<Factuur> HaalFacturenOp();

        List<Product> HaalProductenOp();
    }
}
