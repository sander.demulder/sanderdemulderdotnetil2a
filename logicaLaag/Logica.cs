﻿using dataLaag;
using models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace logicaLaag
{
    public class Logica
    {
        Data _data = new Data();
        Factuur _factuur = new Factuur();

        public List<string> getFacturen()
        {
            return _data.HaalFacturenOp();
        }

        public List<Factuur> getFactuur()
        {
            List<Factuur> listOfFacturen = new List<Factuur>();
            List<string> facturen = getFacturen();

            for(int i = 0; i < facturen.Count; i++)
            {
                listOfFacturen.Add(_factuur.ParseFromLine(facturen[i]));
            }

            return listOfFacturen;
        }

        public List<string> bepaalOrdersPerProductOpBasisVanProductcategorie()
        {
            List<string> outputQuery = new List<string>();

            var result = getFactuur()
            .AsParallel()
            .Where(f => f.ShippedDate.HasValue && f.UnitPrice >= 25 && f.UnitPrice <= 50)
            .OrderBy(f => f.OrderId);


            foreach ( var item in result )
            {
                outputQuery.Add("$\"Order: {factuur.OrderId} - Shipping date: {factuur.ShippedDate} - Product: {factuur.ProductName} - Unit price: € {factuur.UnitPrice:F2}\"");
            }

            return outputQuery;
        }

        public void berekenTotaalPrijsPerOrder()
        {
            int currentYear = DateTime.Now.Year;

            var result = getFactuur()
            .AsParallel()
            .Where(f => f.OrderDate.Year == currentYear)
            .GroupBy(f => f.OrderId)
            .Select(group => new
            {
                OrderId = group.Key,
                CompanyName = group.First().CompanyName,
                OrderDate = group.First().OrderDate,
                TotalPrice = group.Sum(f => f.Quantity * f.UnitPrice * (1 - f.Discount))
            })
            .OrderBy(resultItem => resultItem.OrderId);

        }
    }
}
