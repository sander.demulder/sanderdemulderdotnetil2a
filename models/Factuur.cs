﻿using System.Globalization;

namespace models
{
    public class Factuur
    {
        public string CompanyName { get; set; } = string.Empty;
        public string Address { get; set; } = string.Empty;
        public string City { get; set; } = string.Empty;
        public string StateAbbrev { get; set; } = string.Empty;
        public string Zip { get; set; } = string.Empty;
        public int OrderId { get; set; }
        public int EmployeeId { get; set; }
        public int CustomerId { get; set; }
        public DateOnly OrderDate { get; set; }
        public DateOnly? InvoiceDate { get; set; }
        public DateOnly? ShippedDate { get; set; }
        public int? ShipperId { get; set; }
        public double? ShippingFee { get; set; }
        public double TaxRate { get; set; }
        public int TaxStatusId { get; set; }
        public string PaymentMethod { get; set; } = string.Empty;
        public DateOnly? PaidDate { get; set; }
        public string Notes { get; set; } = string.Empty;
        public int OrderStatusId { get; set; }
        public string AddedBy { get; set; } = string.Empty;
        public DateOnly AddedOn { get; set; }
        public string ModifiedBy { get; set; } = string.Empty;
        public DateOnly ModifiedOn { get; set; }
        public string SalesPerson { get; set; } = string.Empty;
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public double UnitPrice { get; set; }
        public double Discount { get; set; }
        public string ProductCode { get; set; } = string.Empty;
        public string ProductName { get; set; } = string.Empty;

        public static Factuur ParseFromLine(string line)
        {
            string[] values = line.Split('\t');

            if (values.Length < 30)
            {
                throw new ArgumentException("Ongeldige lengte van de invoerarray.", nameof(line));
            }

            Factuur factuur = new Factuur
            {
                CompanyName = GetValueOrDefault(values, 0),
                Address = GetValueOrDefault(values, 1),
                City = GetValueOrDefault(values, 2),
                StateAbbrev = GetValueOrDefault(values, 3),
                Zip = GetValueOrDefault(values, 4),
                OrderId = ParseInt(values[5]),
                EmployeeId = ParseInt(values[6]),
                CustomerId = ParseInt(values[7]),
                OrderDate = ParseDate(values[8], "d/M/yyyy"),  // Aangepaste datumnotatie
                InvoiceDate = ParseNullableDate(values[9], "d/M/yyyy"),  // Aangepaste datumnotatie
                ShippedDate = ParseNullableDate(values[10], "d/M/yyyy"),  // Aangepaste datumnotatie
                ShipperId = ParseNullableInt(values[11]),
                ShippingFee = ParseNullableDouble(values[12]),
                TaxRate = ParseDouble(values[13]),
                TaxStatusId = ParseInt(values[14]),
                PaymentMethod = GetValueOrDefault(values, 15),
                PaidDate = ParseNullableDate(values[16], "d/M/yyyy"),  // Aangepaste datumnotatie
                Notes = GetValueOrDefault(values, 17),
                OrderStatusId = ParseInt(values[18]),
                AddedBy = GetValueOrDefault(values, 19),
                AddedOn = ParseDate(values[20], "d/M/yyyy"),  // Aangepaste datumnotatie
                ModifiedBy = GetValueOrDefault(values, 21),
                ModifiedOn = ParseDate(values[22], "d/M/yyyy"),  // Aangepaste datumnotatie
                SalesPerson = GetValueOrDefault(values, 23),
                ProductId = ParseInt(values[24]),
                Quantity = ParseInt(values[25]),
                UnitPrice = ParseDouble(values[26]),
                Discount = ParseDouble(values[27]),
                ProductCode = GetValueOrDefault(values, 28),
                ProductName = GetValueOrDefault(values, 29)
            };

            return factuur;
        }

        private static string GetValueOrDefault(string[] values, int index)
        {
            return index < values.Length ? values[index] : string.Empty;
        }

        private static int ParseInt(string value)
        {
            return string.IsNullOrEmpty(value) ? 0 : int.Parse(value);
        }

        private static double? ParseNullableDouble(string value)
        {
            return string.IsNullOrEmpty(value) ? (double?)null : double.Parse(value);
        }

        private static DateTime ParseDate(string value, string dateFormat)
        {
            if (DateTime.TryParseExact(value, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime parsedDate))
            {
                return parsedDate;
            }

            throw new FormatException($"Kan de datum '{value}' niet parsen met het opgegeven formaat '{dateFormat}'.");
        }

        private static DateTime? ParseNullableDate(string value, string dateFormat)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            if (DateTime.TryParseExact(value, dateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime parsedDate))
            {
                return parsedDate;
            }

            throw new FormatException($"Kan de datum '{value}' niet parsen met het opgegeven formaat '{dateFormat}'.");
        }


        private static int? ParseNullableInt(string value)
        {
            return string.IsNullOrEmpty(value) ? (int?)null : int.Parse(value);
        }
    }
}
}
