﻿namespace models
{
    public class Product
    {
        public int ProductCategoryId { get; set; }
        public string ProductCategoryName { get; set; } = string.Empty;
        public string ProductCategoryCode { get; set; } = string.Empty;
        public string ProductName { get; set; } = string.Empty;
        public int ProductId { get; set; }
        public string QuantityPerUnit { get; set; } = string.Empty;
        public double UnitPrice { get; set; }
        public double TotalSales { get; set; }
        public string ProductCategoryDesc { get; set; } = string.Empty;
        public string ProductCategoryImage { get; set; } = string.Empty;
    }
}
